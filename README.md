# Cavity QED Molecular Dynamics

Fast C++ and Cuda implementation of molecular dynamics within strong coupling quantum electrodynamical (QED) cavity.